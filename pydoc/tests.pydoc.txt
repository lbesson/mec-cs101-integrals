Help on module tests:

NAME
    tests

FILE
    /home/lilian/teach/mec-cs101-integrals/tests.py

DESCRIPTION
    Examples of several algorithm for numerical integration problems.
    See the `integrals.py <integrals.html>`_ file for more details.
    
    
    - *Date:* Saturday 18 June 2016, 18:59:23.
    - *Author:* `Lilian Besson <https://bitbucket.org/lbesson/>`_ for the `CS101 course <http://perso.crans.org/besson/cs101/>`_ at `Mahindra Ecole Centrale <http://www.mahindraecolecentrale.edu.in/>`_, 2015.
    - *Licence:* `MIT Licence <http://lbesson.mit-license.org>`_, © Lilian Besson.

DATA
    absolute_import = _Feature((2, 5, 0, 'alpha', 1), (3, 0, 0, 'alpha', 0...
    division = _Feature((2, 2, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0), 8192...
    print_function = _Feature((2, 6, 0, 'alpha', 2), (3, 0, 0, 'alpha', 0)...


