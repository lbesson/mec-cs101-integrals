
Some examples of numerical integrations:

Plotting and computing the integral of cos(x) from 0 to pi.
Formally, the value is about 0 (= sin(pi) - sin(0)).
With 15 left rectangles: 0.20943951023931986
With 15 right rectangles: -0.20943951023931923
With 15 center rectangles: 2.918539511321691e-16
With 3 trapezoides: 2.9065570816738595e-16

For this function f1: x → x, on I = [1, 6], we will pick 10000 random points.
Manually, I chose ymin = 1, ymax = 6.
This leads to an approximated integral value of 17.58.
Error % is 7.9999999999998295
But experimentally, I found a possible ymin, ymax to be 0.995, 6.029999999999999.
This leads to a second approximated integral value of 17.466834999999996.
Error % is 3.3165000000003886
Formally, we compute the integral as 17.5 (36/2 - 1/2).

For this function f2: x → x^3, on I = [0.0, 1.0], we will pick 10000 random points.
Manually, I chose ymin = 0.0, ymax = 1.0.
This leads to an approximated integral value of 0.2579.
Error % is 0.7900000000000018
But experimentally, I found a possible ymin, ymax to be 0.0, 1.005.
This leads to a second approximated integral value of 0.24944099999999997.
Error % is 0.05590000000000317
Formally, we compute the integral as 0.25 (1/4).

For this function f3: x → 1/(1+sinh(2x)log(x)^2) on I = [1e-08, 3.0], we will pick 10000 random points.
Manually, I chose ymin = 0, ymax = 1.
This leads to an approximated integral value of 1.1906999960309999.
But experimentally, I found a possible ymin, ymax to be 0.0040756109901336855, 1.004999631063955.
This leads to a second approximated integral value of 1.1815062692822849.
